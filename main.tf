resource "aws_instance" "OneServer" {
  ami                    = var.ec2_image
  instance_type          = var.ec2_instance_type
  count                  = var.ec2_count
  vpc_security_group_ids = ["sg-0b2b7781aa4d5f6f7", "sg-0ac6cb0c0f2eea029"]
  key_name               = "aws-key"
  tags = {
    Name = var.ec2_tags
  }
}

output "instance_ip_addr" {
  value       = aws_instance.OneServer.*.private_ip
  description = "The private IP address of the main server instance."
}

output "instance_ips" {
  value = aws_instance.OneServer.*.public_ip
}


