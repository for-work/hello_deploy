variable "ec2_region" {
  default = "eu-west-3"
}

variable "ec2_image" {
  default = "ami-09e513e9eacab10c1"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_tags" {
  default = "Terraform-ubuntu"
}

variable "ec2_count" {
  default = "1"
}
